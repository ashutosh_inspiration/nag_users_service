﻿
using Nag.Users.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nag.Users.Core.DTOs
{
    public class OrderDetailDto
    {
        public UserDto UsersDeatil { get; set; }
        public IEnumerable<OrderDto> Orders { get; set; }

    }

}
