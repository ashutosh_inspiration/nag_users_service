﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nag.Users.Core.DTOs
{
    public class UserDto
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

    }
}
