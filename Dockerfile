FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY ["Nag.Users.Service/Nag.Users.Service.csproj", "Nag.Users.Service/"]
COPY ["Nag.Users.Data/Nag.Users.Data.csproj", "Nag.Users.Data/"]
COPY ["Nag.Users.Core/Nag.Users.Core.csproj", "Nag.Users.Core/"]
RUN dotnet restore "Nag.Users.Service/Nag.Users.Service.csproj"
COPY . .

WORKDIR "/app/Nag.Users.Service"

RUN dotnet publish "Nag.Users.Service.csproj" -c Release -o /app/out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
EXPOSE 80/tcp
EXPOSE 443/tcp
EXPOSE 8093
EXPOSE 8093/tcp
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "Nag.Users.Service.dll"]