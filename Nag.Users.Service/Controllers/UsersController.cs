using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Nag.Users.Data;
using Nag.Users.Core.DTOs;
using Nag.Users.Core.Models;

namespace Nag.Users.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        public MySqlDataContext Context { get; }

        public UsersController(MySqlDataContext context)
        {
            Context = context;
        }

        [HttpGet]
        public ActionResult<UserDto> Get()
        {
            return this.Context.Users.Select(x => new UserDto() { Name = x.Name, Age = x.Age, Email = x.Email, Id = x.Id }).FirstOrDefault();
        }
        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<UserDto> Get(int id)
        {
            return this.Context.Users.Select(x => new UserDto() { Name = x.Name, Age = x.Age, Email = x.Email, Id = x.Id }).FirstOrDefault(x => x.Id == id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] UserDto x)
        {
            this.Context.Users.Add(new User() { Name = x.Name, Age = x.Age, Email = x.Email, Id = x.Id });
            this.Context.SaveChanges();
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] UserDto x)
        {
            var user = this.Context.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
            {

                user.Name = x.Name;
                user.Age = x.Age;
                user.Email = x.Email;
                user.Id = x.Id;
                this.Context.SaveChanges();
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var user = this.Context.Users.FirstOrDefault(x => x.Id == id);
            if (user != null)
            {
                this.Context.Users.Remove(user);
                this.Context.SaveChanges();
            }
        }
    }
}
